# [Kangaroo](https://dbkangaroo.github.io)
a database management tool for SQLite / MySQL / PostgreSQL on Windows / MacOS / Linux.

# Support database
Database support capability level: __Planned__ / __Partial__ / __Full(:100:)__

| Database    | Version | Query     | Editing   | Designer  | Export    | Import    | Hint      | Modeling | DB Sync |
|-------------|---------|-----------|-----------|-----------|-----------|-----------|-----------|----------|---------|
| SQLite      | 3.x     | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Planned   | Planned  | Planned |
| MySQL       | 8.0     | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Planned   | Planned  | Planned |
| MariaDB     | 10.x    | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Planned   | Planned  | Planned |
| PostgreSQL  | 11.x    | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Planned   | Planned  | Planned |
| Redis       | 5.x     | Planned   | Planned   | Planned   | Planned   | Planned   | Planned   | Planned  | Planned |
| Oracle      |         |           |           |           |           |           |           |          |         |
| SQL Server  |         |           |           |           |           |           |           |          |         |

**Hint**: Code intellisense or Code autocomplete


# Release
Development version will be released weekly, Stable and LTS(Long-term Support) version depend on test result and stabilization.

| Platform | Linux(64 bit)   | Windows(64 bit) | MacOS(64 bit)   |
|----------|-----------------|-----------------|-----------------|
| Stable(current) | On going......  | On going......  | On going......  |
| Development | [download(v0.10.0.191223)](https://dbkangaroo.github.io/download/v0.10.0.191223) | [download(v0.10.0.191223)](https://dbkangaroo.github.io/download/v0.10.0.191223) | [download(v0.10.0.191223)](https://dbkangaroo.github.io/download/v0.10.0.191223) |


# Support the Project
If you like Kangaroo and you want to support its development, pls scan QR code to donate via PayPal / Wechat / Alipay.

![Support project](./images/pay_wide.png)

# Screenshots
![Start page of connection](./images/kangaroo-start.png)
![Tools page of connection](./images/kangaroo-tools.png)
![Kangaroo grid view in table with custom columns](./images/kangaroo-grid.png)
![Kangaroo grid view in table with where statement](./images/kangaroo-grid2.png)
![Kangaroo grid view in form](./images/kangaroo-form.png)
![Kangaroo query view](./images/kangaroo-query.png)
![Kangaroo schema designer form](./images/kangaroo-designer.png)
![Kangaroo schema designer form with preview](./images/kangaroo-designer2.png)
![Kangaroo export assistant](./images/kangaroo-export.png)
![Kangaroo import assistant](./images/kangaroo-import.png)